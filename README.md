# elementary startpage/homepage

A fork of the startpage originally developed by Japorized. The main difference from the original is that this new version
runs as a browser extension rather than being hosted on a web server.

---

## About the page

#### Goals (original version)

* Clean codebase
* Minimal & aesthetically pleasing design
* Compatible with any vi-keybinding emulator for browsers (e.g. Vimium)
* Easy to navigate even with a mouse
* Mobile-friendly

#### Goals (extension version)

* To make the process of adding new bookmarks faster and easier.

#### Usage tips

* Auto-focus on search bar upon visit
* Hit '!' to focus on search bar if it is not already focused
* Search bar redirects to Duckduckgo; [bangs](https://duckduckgo.com/bang?q=) work!
* Hovering over tabs shows the categorized bookmarks
* In mobile view, click on tabs to show bookmarks; vertical scrolling of tabs is possible
* To change theme type `\config` in the search bar.

Each tab on the page is loaded from a bookmark folder. For a folder to be recognized as a bookmark tab
it must be named following the below format: `name | e:color`
  
The name is the text displayed on the tab, the color must be one of six values: red, blue, magenta, cyan, yellow or green.
In order to use an icon instead of text as the name you should simply use the font-awesome icon classname (e.g. `fa-code`).
Example folder name: `fa-code | e:magenta` 

#### Fonts used

* [Fontawesome](https://fontawesome.com/)
* [VT323](https://fonts.google.com/specimen/VT323)
* [KFhimaji](https://www.freejapanesefont.com/kf-himaji/)

---

If you wish to clone/fork and then develop on your own, here's a quick summary of my work structure. Please read the entire section to decide on whether if you want to develop the page similarly.

I'm using gulp as my task runner.

```bash
gulp watch
```

Automatically converts scss into compressed css and minifies js as the files in `/src/` gets modified. This is convenient for development.

```bash
gulp build
```

Does what `gulp watch` does except that it does not watch the files, and runs only once. This is what I use if I'm just making one small change.

If you wish to use the same tools, make sure that you have `npm` installed, and then run this command in the root directory:

```bash
npm i -D
```

---

To deploy to your own repo so that you can access it via `https://<your gitlab username>.gitlab.io/elementary`, assuming that you still call the repo elementary, remember to add a `.gitlab-ci.yml` file that uses Plain HTML.

---

### Known Issues

Only dev issues

* Cannot get BrowserSync to work properly
