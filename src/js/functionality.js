import * as cm from "./configManager.js";

// Tabs
const btns = document.querySelectorAll('#right > div');
const queryBar = document.querySelector('#left input');

const bangs = [
    {"bang": "apkg", "name": "Archlinux packages"},
    {"bang": "archpkg", "name": "Archlinux packages"},
    {"bang": "arch", "name": "Archlinux wiki"},
    {"bang": "archwiki", "name": "Archlinux wiki"},
    {"bang": "archaur", "name": "Archlinux AUR"},
    {"bang": "aur", "name": "Archlinux AUR"},
    {"bang": "archlinux", "name": "Archlinux Forum"},
    {"bang": "archforum", "name": "Archlinux Forum"},
    {"bang": "w", "name": "Wikipedia"},
    {"bang": "wiki", "name": "Wikipedia"},
    {"bang": "bulba", "name": "Bulbapedia"},
    {"bang": "yt", "name": "YouTube"},
    {"bang": "youtube", "name": "YouTube"},
    {"bang": "ytor", "name": "YouTube on Repeat"},
    {"bang": "youtubeonrepeat", "name": "YouTube on Repeat"},
    {"bang": "gh", "name": "Github"},
    {"bang": "github", "name": "Github"},
    {"bang": "glab", "name": "Gitlab"},
    {"bang": "gitlab", "name": "Gitlab"},
    {"bang": "latexwb", "name": "LaTeX Wikibooks"},
    {"bang": "ctan", "name": "CTAN"},
    {"bang": "crates", "name": "Crates.io"},
    {"bang": "cargo", "name": "Crates.io"},
    {"bang": "rust", "name": "Rust stdlib docs"},
    {"bang": "rustdoc", "name": "Rust docs"},
    {"bang": "js", "name": "MDN JS Documentation"},
    {"bang": "css", "name": "MDN CSS Documentation"},
    {"bang": "csst", "name": "CSS Tricks"},
    {"bang": "r", "name": "Reddit"},
    {"bang": "subr", "name": "Subreddits"},
    {"bang": "subreddit", "name": "Subreddits"},
    {"bang": "dictionaryr", "name": "Dictionary.com (reference)"},
    {"bang": "thes", "name": "Thesaurus.com"},
    {"bang": "thesaurus", "name": "Thesaurus.com"},
    {"bang": "wa", "name": "Wolfram Alpha"},
    {"bang": "wolfram", "name": "Wolfram Alpha"},
    {"bang": "wolf", "name": "Wolfram Alpha"},
    {"bang": "math", "name": "Wolfram Mathworld"},
    {"bang": "mathworld", "name": "Wolfram Mathworld"}
];

for (let i = 0; i < btns.length; i++) {
    btns[i].addEventListener('mouseover', function (e) {
        document.querySelector('#right > .active').classList.remove('active');
        this.classList.add('active');
        document.querySelector('#left > .active').classList.remove('active');
        document.querySelector('#left > #' + this.getAttribute('id') + '-content').classList.add('active');
    });
}

// switch to search tab on !
document.onkeyup = function (e) {
    if (e.shiftKey && e.keyCode === 49) {
        const curActive = document.querySelector('#right > .active');
        if (curActive.getAttribute('id') !== 'search') {
            curActive.classList.remove('active');
            document.querySelector('#right > #search').classList.add('active');
            document.querySelector('#left > .active').classList.remove('active');
            document.querySelector('#left > #search-content').classList.add('active');
        }
        queryBar.focus();
    }
};

// smart search
queryBar.onkeydown = function (e) {
    const $this = this;
    if (e.key === "Enter") {
	if ($this.value.indexOf('\\') === 0) {
		const command = $this.value.substr(1);
		if (command === "toggle-theme") {
			if (localStorage.getItem('theme-override') === "dark")
				localStorage.setItem('theme-override', 'light');
			else localStorage.setItem('theme-override', 'dark');
			checkThemeOverride();
		} else if (command === "config") {
		    cm.openConfig();
        }
		$this.value = "";
	} else if (hasProtocol($this.value)) {
            window.location.href = $this.value;
        } else {
            if (hasName($this.value)) {
                window.location.href = "https://" + $this.value;
            } else if ($this.value.indexOf('/r/') === 0 || $this.value.indexOf('r/') === 0) {
                if ($this.value.indexOf('/') === 0) {
                    window.location.href = "https://reddit.com" + $this.value;
                } else {
                    window.location.href = "https://reddit.com/" + $this.value;
                }
            } else {
                if ($this.value.indexOf('qw ') === 0) { // escape default search to use qwant as search engine
                    window.location.href = "https://lite.qwant.com/?q=" + $this.value.substring(3, $this.length);
                } else { // defaults to searching using ddg
                    window.location.href = "https://duckduckgo.com/?q=" + $this.value;
                }
            }
        }
    } else if (e.keyCode === 9) {
        e.preventDefault();
        if ($this.value === "ht") {
            $this.value = 'https://';
        } else {
            for (let bookmark of bookmark_index) {
                if (bookmark.title.toLowerCase().indexOf($this.value.toLowerCase()) > -1) {
                    $this.value = bookmark.url;
                }
            }
        }
    }
};

queryBar.onkeyup = function (e) {
    if (this.value === "") {
        cleanBangMatches();
    } else if (this.value.charAt(0) === "!") {
        const matches = checkBang(this.value.substring(1));
        showBangMatches(matches);
        if (matches.length === 0) {
            cleanBangMatches();
        } else {
            makeBangMatchesClickable();
        }

        if (e.keyCode === 9) {
            this.value = "!" + matches[0].bang;
        }
    }
};

const bookmark_index = [];

// fill in bookmarks onload
function bookmarks() {
    browser.bookmarks.search("| e:")
        .then(result => {
            for (let bookmark of result) {
                if (bookmark.type !== "folder")
                    continue;
                const title_and_name = /(\S+) \| e:(\w+)/.exec(bookmark.title);

                // Check if the title is a font awesome icon
                const container = document.getElementById(title_and_name[2]);
                container.style.display = 'block';
                if (title_and_name[1].indexOf('fa-') === 0) {
                    container.children[0].innerHTML = `<span class='fa ${title_and_name[1]}'></span>`;
                } else {
                    container.children[0].innerHTML = title_and_name[1];
                }

                browser.bookmarks.getChildren(bookmark.id)
                    .then(children => {
                        const container = document.querySelector(`#${title_and_name[2]}-content .content-container p`);
                        for (const child of children) {
                            const element = document.createElement('a');
                            element.tabIndex = -1;
                            element.href = child.url;
                            element.innerHTML = child.title;
                            container.append(element);
                            container.append(" ");
                            bookmark_index.push({
                                title: child.title,
                                url: child.url,
                                tab: container.children[0].innerHTML
                            })
                        }
                    })
            }
        });
}

// Date & Time
const $datetime_day = document.querySelector('.datetime-day');
const $datetime_time = document.querySelector('.datetime-time');

function clock() {
    let raw_time = new Date();
    let curHours = raw_time.getHours();
    let curMinutes = raw_time.getMinutes();
    let curSeconds = raw_time.getSeconds();
    let curDay = raw_time.getDay();
    let dayStr;

    curHours = (curHours < 10 ? "0" : "") + raw_time.getHours();
    curMinutes = (curMinutes < 10 ? "0" : "") + curMinutes;
    curSeconds = (curSeconds < 10 ? "0" : "") + curSeconds;

    switch (curDay) {
        case 0:
            dayStr = "日";
            break;
        case 1:
            dayStr = "月";
            break;
        case 2:
            dayStr = "火";
            break;
        case 3:
            dayStr = "水";
            break;
        case 4:
            dayStr = "木";
            break;
        case 5:
            dayStr = "金";
            break;
        case 6:
            dayStr = "土";
            break;
        default:
            dayStr = " ";
    }

    let curTimeString;
    if (curSeconds % 2 === 0)
        curTimeString = curHours + ":" + curMinutes + ":" + curSeconds;
    else
        curTimeString = curHours + " " + curMinutes + " " + curSeconds;

    $datetime_time.innerHTML = curTimeString;
    $datetime_day.innerHTML = dayStr;
}

/*
 * BANGS!
 */
function checkBang(query) {
    return bangs.filter(item => item.bang.includes(query));
}

function showBangMatches(matches) {
    const searchWrapper = document.querySelector('#left > #search-content');
    let div = document.querySelector('.bang-matches');
    if (div === null) {
        div = document.createElement("div");
        div.classList.add('bang-matches');
        searchWrapper.appendChild(div);
    }
    let ul = document.querySelector('.bang-matches ul');
    if (ul === null) {
        ul = document.createElement("ul");
        div.appendChild(ul);
    } else {
        while (ul.hasChildNodes()) {
            ul.removeChild(ul.lastChild);
        }
    }
    matches.forEach((match) => {
        const bang = document.createElement("b");
        bang.innerHTML = "!" + match.bang;
        const desc = document.createElement("span");
        desc.innerHTML = match.name;
        const li = document.createElement("li");
        li.appendChild(bang);
        li.appendChild(desc);
        ul.appendChild(li);
    });

}

function makeBangMatchesClickable() {
    const lis = document.querySelectorAll('.bang-matches ul li');
    if ( lis !== null ) {
        lis.forEach((li) => {
            li.addEventListener('click', () => {
                queryBar.value = li.querySelector('b').innerHTML;
            });
        });
    }
}

function cleanBangMatches() {
    const div = document.querySelector('.bang-matches');
    if ( div !== null ) {
        div.parentNode.removeChild(div);
    }
}


/* 
 * UTILITIES
 */
function hasName(val) {
    const names = ['.com', '.org', '.io', '.net', '.ca', '.ink'];
    for (let i = 0; i < names.length; i++) {
        if (val.indexOf(names[i]) > 0) {
            return true;
        }
    }
    return false;
}

function hasProtocol(url) {
    const protocols = ['http://', 'https://', 'file://'];
    for (let i = 0; i < protocols.length; i++) {
        if (url.indexOf(protocols[i]) === 0) {
            return true;
        }
    }
    return false;
}


function startup() {
    const config = cm.loadConfig();
    cm.applyConfig(config);
    bookmarks();
    clock();
    setInterval(clock, 1000);
}

window.addEventListener('load', startup);
