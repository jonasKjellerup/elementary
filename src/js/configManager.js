const config_key = "elementary:config";

let initialised = false;

function save_config() {
    document.getElementById("config-manager").style.display = "none";
    const config = {
        theme: document.getElementById("cfg-select-theme").value,
    };

    localStorage.setItem(config_key, JSON.stringify(config));
    applyConfig(config);
}

export const root = document.documentElement;

export const defaultOptions = {
    theme: 'os',
};

export const darkTheme = {
    "--body-bg-color": "#2d2d2d",
    "--body-fg-color": "#d3d0c8",
    "--search-img-src": "url('/assets/img/city_night_dark.gif')"
};

export const lightTheme = {
    "--body-bg-color": "#fff3cb",
    "--body-fg-color": "#010101",
    "--search-img-src": "url('/assets/img/fish_petshop_routine.gif')"
};

export const duskTheme = {
    "--body-bg-color": "#2d2d2d",
    "--body-fg-color": "#d3d0c8",
    "--search-img-src": "url('/assets/img/city_dusk.gif')"
};

export function openConfig() {
    const config = loadConfig();

    document.getElementById("config-manager").style.display = "block";
    document.getElementById("cfg-select-theme").value = config.theme;

    if (!initialised) {
        document
            .getElementById("config-save")
            .addEventListener("click", save_config);
        initialised = true;
    }
}

export function applyConfig(config) {
    let theme;
    switch (config.theme) {
        case "dark":
            theme = darkTheme;
            break;
        case "light":
            theme = lightTheme;
            break;
        case "dusk":
            theme = duskTheme;
            break;
        case "os":
            break;
        default:
            break;
    }

    for (let property in theme) {
        if (theme.hasOwnProperty(property))
            root.style.setProperty(property, theme[property]);
    }
}

export function loadConfig() {
    const config = localStorage.getItem(config_key);
    if (config == null) return defaultOptions;
    try {
        return JSON.parse(config);
    } catch (_) {
        return defaultOptions;
    }
}

